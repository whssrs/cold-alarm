<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\UserType;
use App\Entity\User;
use App\Service\MailGenerator;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration", name="registration")
     */
    public function register(Request $request,
                             UserPasswordEncoderInterface $passwordEncoder,
                             EntityManagerInterface $em,
                             MailGenerator $mailer)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $factory = new \RandomLib\Factory;
            $generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));
            $plainPassword = $generator->generateString(16);
            $password = $passwordEncoder->encodePassword($user, $plainPassword);

            $confirmLink = md5(uniqid(rand(), true));
            $user->setInitialValues($password, $confirmLink);

            $em->persist($user);
            $em->flush();

            $view = $this->renderView('emails/confirm.html.twig', [
                'password' => $plainPassword,
                'link' => $confirmLink
            ]);
            $mailer->sendEmail($user->getEmail(), 'Завершение регистрации в сервисе Cold Alarm', $view);

            return $this->render('registration/success.html.twig');
        }

        return $this->render('registration/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
