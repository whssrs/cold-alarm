<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class ConfirmationController extends AbstractController
{
    /**
     * @Route("/confirm/{hash}", name="confirm")
     */
    public function index($hash, EntityManagerInterface $em)
    {
        $repository = $em->getRepository(User::CLASS);

        $user = $repository->findOneBy([
            'confirmLink' => $hash,
            'isVerified' => 0
        ]);

        if (!$user) {
            $message = 'Email уже подтвержден или ссылка не верна';
            $type = 'danger';
        } else {
            $user->verify();
            $em->persist($user);
            $em->flush();
            $message = 'Email успешно подтвержден';
            $type = 'success';
        }

        return $this->render('confirmation/index.html.twig', [
            'message' => $message,
            'type' => $type
        ]);
    }
}
