<?php

namespace App\Service;

class MailGenerator
{
    const FROM_EMAIL = 'cold.alarm@mail.ru';

    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail($address, $theme, $body)
    {
        $message = (new \Swift_Message($theme))
            ->setFrom(self::FROM_EMAIL)
            ->setTo($address)
            ->setBody($body, 'text/html');
        $this->mailer->send($message);

    }
}
